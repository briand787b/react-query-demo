import { UserPage } from "./components/userPage";
import { QueryClient, QueryClientProvider } from "react-query";
import { ReactQueryDevtools } from "react-query/devtools";

function App() {
  const queryClient = new QueryClient();

  return (
    <div className="App">
      <QueryClientProvider client={queryClient}>
        <header className="App-header">
          <p>header</p>
        </header>
        <UserPage />
        <footer>
          <p>footer</p>
        </footer>
        <ReactQueryDevtools initialIsOpen={false}/>
      </QueryClientProvider>
    </div>
  );
}

export default App;
