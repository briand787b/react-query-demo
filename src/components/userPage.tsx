import { FC, useState } from "react";
import { Info } from "./info";
import { Login } from "./login";

export const UserPage: FC = () => {
  console.log("UserPage component re-rendered");
  const [search, setSearch] = useState("");

  return (
    <div>
      <Login search={search} setSearch={setSearch} />
      <Info search={search} />
    </div>
  );
};
