import { ChangeEventHandler, Dispatch, FC, FormEventHandler, SetStateAction, useState } from 'react';

export type LoginParams = {
  search: string;
  setSearch: Dispatch<SetStateAction<string>>;
}

export const Login: FC<LoginParams> = ({search, setSearch}) => {
  console.log('Login component rendered')

  const handleSearchChange: ChangeEventHandler<HTMLInputElement> = (e) => {
    setSearch(e.target.value);
  }

  const handleSubmit: FormEventHandler<HTMLFormElement> = (e) => {
    console.log('form submitted');
    e.preventDefault();
  }

  return (
    <div>
      <form onSubmit={handleSubmit}>
        <label>Username</label>
        <input type="text" name="username" value={search} onChange={handleSearchChange}/>
        <button type="submit" value="Submit">Submit</button>
      </form>
    </div>);
};
