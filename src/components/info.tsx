import { FC } from "react";
import { isError, useQuery } from "react-query";
import axios from 'axios';

export type InfoParams = {
  search: string;
};

export const Info: FC<InfoParams> = ({ search }) => {
  console.log("Info component re-rendered");
  const {data: users = [], ...query} = useQuery(['search', search], async () => {
    const response = await axios.get<SearchResponse[]>(`http://localhost:3001/users?q=${search}`);
    return response.data;
  });

  if (query.isError) {
    return <p>Error: {query.error}</p>
  }

  return (
    <div>
      <h3>Info component dispalying {search} results</h3>
      <main>
        {users.map(u => (<p>{u.name}</p>))}
      </main>
    </div>
  );
};

type SearchResponse = {
  id: number;
  name: string;
}